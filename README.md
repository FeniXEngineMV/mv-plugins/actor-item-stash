 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

<a href='https://ko-fi.com/ltngames' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0' border='0' alt='Support the Devs' /></a>

 # Actor Item Stash

 This plugins will provide a new way to interact with your actors inventory
 by granting each actor their very own inventory space! You can trade items
 between your party members, or choose to deposit or widthraw items in the
 global stash which is accessed in a new menu. All items gained through event
 commands are added to a temporary stash which lets you decide what to do with
 an item, all of which can be accessed on the current map.

## Installation</a></h3>

  Place the plugin file directly in your game project's `/js/plugins/`
  directory.

#### Temporary Stash

 Because each actor now has their own inventory space, when using event commands
 like "Change Items", "Change Weapons", and "Change Armors" to add itemsm
 they will be moved  to the temporary item stash instead of the default inventory.
 Now that items are pushed to the temporary item stash you can then use the
 plugin command(s) or script calls()) and let the player decide what to do
 with it's items.

#### Global Item Stash

 A global item stash is now accessible via a brand new scene. This scene will
 display all items stored in the global stash and provides you a way to deposit
 and withdraw items. This scene is a way to provide the player with a to move
 items they don't currently require to a stash that is not accessed through the
 main menu but through an event.

#### Selling/Buying

Selling and buying items at shops now allow you to select an actor before
purchasing or selling. This means all items will be stashed into the selected
actor's item stash.
Only small changes have been made, the first thing to take note of is
before purchasing or selling an item you will be asked to select the actor in
which the item should be added to their inventory space. The second is that the
"possesion" text drawn in the shop will display how many items out of the max amount
allowed to own.

#### Equipping

 Same as buying or selling, you can only equip an item that the actor owns in
 their own item stash. Nothing has changed here except for not being able to
 equip items that are in another actor's inventory space.