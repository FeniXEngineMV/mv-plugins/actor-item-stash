/**
 * All plugin parameters for this plugin.
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributors
 * @copyright    2020 FeniXEngine
 */

/* eslint-disable spaced-comment */

/*:
 * @pluginname X_ActorItemStash
 * @plugindesc Each actor has their own inventory stash, trade with other actors,
 * and have access to a global stash. <X_ActorItemStash>
 *
 *
 * @modulename X_ActorItemStash
 * @version 1.2.1
 * @required
 * @external
 *
 * @author FeniXEngine Contributors (https://fenixenginemv.gitlab.io/)
 *
 * @param compatibilityOptions
 * @text Compatibility Options
 *
 * @param altMenuEnabled
 * @text AltMenu Enabled
 * @parent compatibilityOptions
 * @desc Is AltMenuScreen.js enabled in your project ?
 * @type boolean
 * @default false
 *
 * @param maxGlobalStash
 * @text Global Stash Max
 * @parent generalOptions
 * @desc Max amount of items allowed to be stored in the global stash
 * @type number
 * @default 999
 *
 * @param maxTempStash
 * @text Temp Stash Max
 * @parent generalOptions
 * @desc Max amount of items allowed to be stored in the temp stash
 * @type number
 * @default 99
 *
 * @param actorStashSlots
 * @text Actor Starting Stash Slots
 * @parent generalOptions
 * @desc The starting amount of items allowed to be stored in the actors stash.
 * @type number
 * @default 99
 *
 * @param actorItemStack
 * @text Actor Starting Item Stack
 * @parent generalOptions
 * @desc The starting amount an item which can be stacked upon itself for actors stash.
 * @type number
 * @default 99
 *
 * @param forceStackAsSlots
 * @text Count Stack As slots
 * @parent generalOptions
 * @desc Force each item in a stack to count towards your total item stash slots
 * @type boolean
 * @default true
 *
 * @param addGlobalStashToMenu
 * @text Add global Stash To Menu
 * @parent generalOptions
 * @desc Adds the global stash scene as an option in the main menu for quick access
 * @type boolean
 * @default false
 *
 * @param globalMenuName
 * @text Global Stash Menu Entry Name
 * @parent generalOptions
 * @desc The name to use for the global stash menu entry
 * @type string
 * @default Global Stash
 *
 * @param autoAddAfterBattle
 * @text Auto Add After Battle
 * @parent generalOptions
 * @desc Auto add all item rewards after battle to the first available slots across actors and global stash.
 * @type boolean
 * @on Auto Add
 * @off Temp Stash
 * @default false
 *
 * @param generalOptions
 * @text General Options
 *
 * @param tempOptions
 * @text Temp Stash Options
 *
 * @param tempStashCancelAction
 * @text Temp Stash Cancel Action
 * @parent tempOptions
 * @desc The action to perform when the user cancels the temp stash window
 * @type select
 * @option Discard all items
 * @value discard
 * @option Auto add
 * @value auto
 * @option Disallow cancelling
 * @value disallow
 * @default discard
 *
 * @param tempStashWindow
 * @text TempStash Window Options
 * @parent tempOptions
 * @desc Options for the temporary item stash window
 * @type struct<CommandWindowOptions>
 * @default {"x":"0","y":"0","width":"350","maxCols":"1","numVisibleRows":"","fontSize":"24"}
 *
 * @param tempActorSelectWindow
 * @text TempStash Actor Select Window Options
 * @parent tempOptions
 * @desc Options for the temporary item stash actor select window
 * @type struct<CommandWindowOptions>
 * @default {"x":"0","y":"0","width":"350","maxCols":"1","numVisibleRows":"","fontSize":"24"}
 *
 * @param defaultHelpTerms
 * @text Default Help Terms
 * @parent generalOptions
 * @type struct<HelpTerms>
 * @desc The default help terms used when selecting commands
 * @default {"default":"\\i[4] What would you like to do?","use":"\\i[209] Use item in your inventory","discard":"\\i[194] Discard items to be gone forever","trade":"\\i[142] Trade items with another party member","withdraw":"\\i[73] Withdraw items from the global stash","deposit":"\\i[74] Deposit items to the global stash","actorSelect":"\\i[4] Choose another party member"}
 *
 * @param defaultTerms
 * @text Default Terms
 * @parent generalOptions
 * @type struct<Terms>
 * @desc The default terms used for commands in actor, global and temporary stash windows
 * @default {"useTerm":"Use","discardTerm":"Discard","tradeTerm":"Trade","withdrawTerm":"Withdraw","depositTerm":"Deposit","autoTerm":"Auto","quantityTerm":"How many"}
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 MIT License -

 * Free for use in any RPG Maker MV game project, commerical or otherwise

 * Credit may go to FeniXEngine Contributers or FeniXEngine

 * Though not required, you may provide a link back to the original source code,
   repository or website.

 -------------------------------------------------------------------------------
 # Installation

  Place the plugin file directly in your game project's `/js/plugins/`
  directory

 # INFORMATION

 This plugins will provide a new way to interact with your actors inventory
 by granting each actor their very own inventory space! You can trade items
 between your party members, or choose to deposit or widthraw items in the
 global stash which is accessed in a new menu. All items gained through event
 commands are added to a temporary stash which lets you decide what to do with
 an item, all of which can be accessed on the current map.

 Please read the following before proceeding to use the plugin so you have a
 good understanding of the nature of the plugin and how it works.

 ## Temporary Stash

 Because each actor now has their own inventory space, when using event commands
 like "Change Items", "Change Weapons", and "Change Armors" to add items will
 be moved  to the temporary item stash instead of the default inventory.
 Now that items are pushed to the temporary item stash you can then use the
 plugin command(s) or script calls()) below to open the temporary stash
 and let the player decide what to do with it's items.

 ## Global Item Stash

 A global item stash is now accessible via a brand new scene. This scene will
 display all items stored in the global stash and provides you a way to deposit
 and withdraw items. This scene is a way to provide the player with a to move
 items they don't currently require to a stash that is not accessed through the
 main menu but through an event.

 ## Selling/Buying

Selling and buying items at shops now allow you to select an actor before
purchasing or selling. This means all items will be stashed into the selected
actor's item stash.
Only small changes have been made, the first thing to take note of is
before purchasing or selling an item you will be asked to select the actor in
which the item should be added to their inventory space. The second is that the
"possesion" text drawn in the shop will display how many items out of the max amount
allowed to own.

 ## Equipping

 Same as buying or selling, you can only equip an item that the actor owns in
 their own item stash. Nothing has changed here except for not being able to
 equip items that are in another actor's inventory space.

 ## Actor Notetags

<ActorStash>
starting Slots: amount
startingStack: amount
</ActorStash>

 Starting slots allows you to set how many slots an actor has initially upon a
 new game

 Starting stack allows you to set how much an actor can stack per item

 ## Parameters

 Be sure to go through all parameters and read their descriptions for even more
 customizations!

 ## Plugin Commands (Case insensitive)

 ItemStash OpenGlobalStash
 - Opens the global item stash scene

 ItemStash OpenTempStash
 - Opens the temp item stash windows on map

 ItemStash SetMaxStash actorId amount
 - Sets the actors max amount of item slots

 ItemStash SetMaxGlobalStash amount
 - Sets the global stash's max amount of item slots

 ItemStash SetMaxTempStash amount
 - Sets the temporary stash's max amount of item slots

 ItemStash AddToMaxStack actorId amount
 - Adds to the actors max amount of items in a stack

 ItemStash AddToMaxSlots actorId amount
 - Adds  to the amount of items an actor can hold

 ItemStash AddToActor actorId itemType itemId amount
 - Adds item(s) to an actor's item stash

 ItemStash RemoveFromActor actorId itemType itemId amount
 - Removes item(s) from an actor's item stash

 ItemStash AddToGlobal itemType itemId amount
 - Adds item(s) to the global stash

 ItemStash RemoveFromGlobal itemType itemId amount
 - Removes item(s) from the global stash

 ItemStash AddToTemp itemType itemId amount
  - Adds item(s) to the temp item stash

 ItemStash RemoveFromTemp itemType itemId amount
  - Removes item(s) from the temp item stash

 ## Script Calls

 $gameActors.actor(actorId).addToStash()
 - Adds an item to the actors stash.

 $gameActors.actor(actorId).removeFromStash()
 - Removes an item from the actors stash.

 $gameActors.actor(actorId).numItemsInStash()
 - Returns the amount of an item available in the actors stash.

 $gameActors.actor(actorId).canStashItems()
  - Returns true if the actor can stash an item.

 $gameParty.openTempStash()
 - Opens the temporary item stash.

 $gameParty.closeTempStash()
 - Close the temporary item stash.

 $gameParty.numItemsInTempStash({id: 1, type: 'item'})
 - Returns the amount of the item in temp stash.

 $gameParty.numItemsInGlobalStash({id: 1, type: 'item'})
 - Returns the amount of the item in global stash.

 $gameParty.addToTempStash({id: 1, type: 'item'}, amount)
 - Adds an item to the temp stash.

 $gameParty.removeFromTempStash({id: 1, type: 'item'}, amount)
 - Removes an item from the temp stash.

 $gameParty.addToGlobalStash({id: 1, type: 'item'}, amount)
 - Adds an item to the global stash.

 $gameParty.removeFromGlobalStash({id: 1, type: 'item'}, amount)
 - Removes an item from the global stash.

 $gameParty.autoAddToStash({id: 1, type: 'item'}, amount)
 - Automatically adds an item to the first stash which has available space
   First it searches each actor stash and if it can't find available space it
   will place it in the global stash.

 $gameParty.autoRemoveFromStash({id: 1, type: 'item'}, amount)
  - Finds the first occurrence of the item and removes it.

 $gameParty.numItems({id: 1, type: 'item'})
 - Returns the amount of an item across all actors.

 $gameParty.allItems()
 - Returns all items available across all actors.

*/

/*~struct~Terms:
 * @param useTerm
 * @text Use Term
 * @type string
 * @desc The term used for the use item command
 * @default Use
 *
 * @param discardTerm
 * @text Discard Term
 * @type string
 * @desc The term used for the discard item command
 * @default Discard
 *
 * @param tradeTerm
 * @text Trade Term
 * @type string
 * @desc The term used for the trade item command
 * @default Trade
 *
 * @param withdrawTerm
 * @text Withdraw Term
 * @type string
 * @desc The term used for the withdraw item command
 * @default Withdraw
 *
 * @param depositTerm
 * @text Deposit Term
 * @type string
 * @desc The term used for the deposit item command
 * @default Deposit
 *
 * @param autoTerm
 * @text Auto Term
 * @desc The term used for the Auto stash items command
 * @type string
 * @default Auto
 *
 * @param quantityTerm
 * @text Quantity Term
 * @desc The term used for the quantity confirm window.
 * @type string
 * @default How many?
 *
 * @param actorMaxItemsTerm
 * @text Actor Max Item Term
 * @desc The term used for the item's scene status window which displays the
 * actor's max amount of items
 * @type string
 * @default Max Item Slots:
 *
 * @param actorItemsUsedTerm
 * @text Actor Item Used Term
 * @desc The term used for the item's scene status window which displays the
 * actor's amount of items
 * @type string
 * @default Item Slots Used:
 *
 * @param actorItemStackTerm
 * @text Actor Item Stack Term
 * @desc The term used for the item's scene status window which displays the
 * actor's max stack for each item.
 * @type string
 * @default Max Item Stack:
 *
 * @param globalMaxItemsTerm
 * @text Actor Max Item Term
 * @desc The term used for the global stash scene's status window which displays the
 * global stash's max amount of items
 * @type string
 * @default Max Item Slots:
 *
 * @param globalItemsUsedTerm
 * @text Actor Item Used Term
 * @desc The term used for the global stash scene's status window which displays the
 * global stash amount of items stored
 * @type string
 * @default Item Slots Used:
 *
 */

/*~struct~HelpTerms:
 * @param default
 * @text Default Term
 * @type string
 * @desc The term used when no command is selected
 * @default What would you like to do?
 *
 * @param use
 * @text Use Term
 * @type string
 * @desc The term used when the use item command is selected
 * @default Use your items
 *
 * @param discard
 * @text Discard Term
 * @type string
 * @desc The term used when the use discard command is selected
 * @default Discard items to be gone forever
 *
 * @param trade
 * @text Trade Term
 * @type string
 * @desc The term used when the use trade command is selected
 * @default Trade items with another party member
 *
 * @param withdraw
 * @text Withdraw Term
 * @type string
 * @desc The term used when the use withdraw command is selected
 * @default Withdraw items from the global stash
 *
 * @param deposit
 * @text Deposit Term
 * @type string
 * @desc The term used when the use deposit command is selected
 * @default Deposit items to the global stash
 *
 * @param actorSelect
 * @text Actor Select Term
 * @type string
 * @desc The term used when selecting an actor
 * @default Choose another party member
 *
 */

/*~struct~CommandWindowOptions:
 * @param x
 * @text X Position
 * @type number
 * @desc The position of the window on the x axis
 * @default 0
 *
 * @param y
 * @text Y Position
 * @type number
 * @desc The position of the window on the y axis
 * @default 0
 *
 * @param width
 * @text Width
 * @type number
 * @desc The width of the window
 * @default 400
 *
 * @param maxCols
 * @text Max Columns
 * @type number
 * @desc The max amount of columns to display in the window.
 * @default 1
 *
 * @param numVisibleRows
 * @text Number Of Rows
 * @type number
 * @desc The max amount of rows to display in the window before items in the list
 * require scrolling.
 * @default
 *
 * @param fontSize
 * @text Font Size
 * @type number
 * @desc The default font size for the content in the window
 * @default
 *
 */
