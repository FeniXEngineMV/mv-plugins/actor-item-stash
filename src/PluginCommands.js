import { _Aliases } from './Core'
import Scene_GlobalStash from './Scenes/Scene_GlobalStash'

_Aliases.gameInterpreterPluginCommand = Game_Interpreter.prototype.pluginCommand

Game_Interpreter.prototype.pluginCommand = function (command, args) {
  if (command.toLowerCase() === 'itemstash') {
    const actor = $gameActors.actor(Number(args[1])) || null
    const actorStashItem = { type: args[2], id: Number(args[3]) }
    const actorAmount = Number(args[4])

    const partyStashItem = { type: args[1], id: Number(args[2]) }
    const partyAmount = Number(args[3])

    switch (args[0].toLowerCase()) {
      case 'openglobalstash':
        SceneManager.push(Scene_GlobalStash)
        break
      case 'opentempstash':
        $gameParty.openTempStash()
        break
      case 'addtomaxstack':
        actor.setMaxItemStack(actor.maxItemStack() + Number(args[2]))
        break
      case 'addtomaxslots':
        actor.setMaxStashSlots(actor.maxItemStashSlots() + Number(args[2]))
        break
      case 'setmaxstash':
        actor.setMaxStashSlots(args[2])
        break
      case 'setmaxglobalstash':
        actor.setMaxGlobalStashSlots(args[1])
        break
      case 'setmaxtempstash':
        $gameParty.setMaxTempStash(args[1])
        break
      case 'addtoactor':
        actor.addToStash(actorStashItem, actorAmount)
        break
      case 'removefromactor':
        actor.removeFromStash(actorStashItem, actorAmount)
        break
      case 'addtoglobal':
        $gameParty.addToGlobalStash(partyStashItem, partyAmount)
        break
      case 'removefromglobal':
        $gameParty.removeFromGlobalStash(partyStashItem, partyAmount)
        break
      case 'addtotemp':
        $gameParty.addToTempStash(partyStashItem, partyAmount)
        break
      case 'removefromtemp':
        $gameParty.removeFromTempStash(partyStashItem, partyAmount)
        break
      default:
        throw new Error('Unable to find plugin command')
    }
  } else {
    _Aliases.gameInterpreterPluginCommand.call(this, command, args)
  }
}
