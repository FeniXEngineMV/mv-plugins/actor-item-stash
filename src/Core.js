import { convertParameters, getMultiLineTag } from 'fenix-tools'

export const _Params = convertParameters($plugins.filter(
  plugin => plugin.description.contains('<X_ActorItemStash>'))[0].parameters)

export const _Aliases = {}

export const _Notetags = { actors: [] }

function parseStringValue (string) {
  return isFinite(string) ? Number(string) : string
}

function toObject (string) {
  const properties = string.split('\n')
  const obj = {}
  properties.forEach((property) => {
    const pair = property.split(':')
    const key = pair[0].trim()
    const value = parseStringValue(pair[1].trim())
    obj[key] = value
  })
  return obj
}

function getActorNotetags (tag) {
  return $dataActors.map(actor => {
    if (actor === null) {
      return null
    }
    const notetag = getMultiLineTag(actor.note, tag || 'ActorStash')[0]
    if (notetag) {
      return toObject(notetag)
    }
  })
}

const oldDataManagerIsDatabaseLoaded = DataManager.isDatabaseLoaded
DataManager.isDatabaseLoaded = function () {
  if (!oldDataManagerIsDatabaseLoaded.call(this)) {
    return false
  }
  // We filter because the returned value contains undefined elements
  _Notetags.actors = getActorNotetags('ActorStash')
  return true
}
