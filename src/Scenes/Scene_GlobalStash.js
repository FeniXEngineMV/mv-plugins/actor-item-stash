import { _Params } from '../Core'
import Scene_ItemStashBase from '../Scenes/Scene_ItemStashBase'

import Window_GlobalCommands from '../Windows/GlobalStash/Window_GlobalCommands'
import Window_ItemStashCategory from '../Windows/ItemStash/Window_ItemStashCategory'
import Window_ItemStash from '../Windows/ItemStash/Window_ItemStash'
import Window_ItemStatus from '../Windows/Window_ItemStatus'
/**
 * A new Scene to handle new actor stash/trading and discarding of items
 */
export default class Scene_GlobalStash extends Scene_ItemStashBase {
  constructor () {
    super()
    super.initialize()
    this.updateActor()
    this._activeWindow = null
  }

  start () {
    super.start()
    this._statusWindow.refresh()
  }

  updateHelp () {
    if (this._commandWindow &&
        (this._commandWindow.active || this._categoryWindow.active)) {
      const helpTerms = _Params.defaultHelpTerms

      switch (this._commandWindow.currentSymbol()) {
        case 'withdraw':
          this._helpWindow.setText(helpTerms.withdraw)
          break
        case 'deposit':
          this._helpWindow.setText(helpTerms.deposit)
          break
        case 'discard':
          this._helpWindow.setText(helpTerms.discard)
          break

        default:
          this._helpWindow.setText(helpTerms.default)
          break
      }
    }
  }

  create () {
    super.create()
    this.createCommandsWindow()
    this.createStatusWindow()
    this.createCategoryWindow()
    this.createItemWindow()
    this.createActorItemWindow()
    this.createActorSelectWindow()
    this.createQuantityWindow()
    this._actorSelectWindow.setForItemUse(true)
  }

  createCommandsWindow () {
    this._commandWindow = new Window_GlobalCommands()
    this._commandWindow.setHelpWindow(this._helpWindow)
    this._commandWindow.y = this._helpWindow.height
    this._commandWindow.x = 0

    this._commandWindow.setHandler('ok', this.onOkCommand.bind(this))
    this._commandWindow.setHandler('cancel', this.popScene.bind(this))
    this.addWindow(this._commandWindow)
  }

  createCategoryWindow () {
    this._categoryWindow = new Window_ItemStashCategory()
    this._categoryWindow.setHelpWindow(this._helpWindow)
    this._categoryWindow.setItemCommandWindow(this._commandWindow)
    this._categoryWindow.y = this._helpWindow.height
    this._categoryWindow.x = 0
    this._categoryWindow.hide()
    this._categoryWindow.deactivate()
    this._categoryWindow.setHandler('ok', this.onCategoryOk.bind(this))
    this._categoryWindow.setHandler('cancel', this.onCategoryCancel.bind(this))
    this._categoryWindow.setHandler('pagedown', this.nextActor.bind(this))
    this._categoryWindow.setHandler('pageup', this.previousActor.bind(this))
    this.addWindow(this._categoryWindow)
  }

  createStatusWindow () {
    const wx = this._commandWindow.width
    const wy = this._helpWindow.height
    const ww = Graphics.boxWidth - wx
    const wh = this._commandWindow.height
    this._statusWindow = new Window_ItemStatus(wx, wy, ww, wh)
    this._statusWindow.reserveFaceImages()
    this._statusWindow.setForGlobalStash()
    this.addWindow(this._statusWindow)
  }

  createItemWindow () {
    const wy = this._commandWindow.y + this._commandWindow.height
    const wh = Graphics.boxHeight - wy
    this._itemWindow = new Window_ItemStash(0, wy, Graphics.boxWidth, wh)
    this._itemWindow.setHelpWindow(this._helpWindow)
    this._itemWindow.setHandler('ok', this.onItemOk.bind(this))
    this._itemWindow.setHandler('cancel', this.onItemCancel.bind(this))
    this._itemWindow.setStashType('global')
    this._itemWindow.setAlwaysEnabled(true)
    this._itemWindow.setShowAllItems(true)
    this.addWindow(this._itemWindow)
    this._categoryWindow.setItemWindow(this._itemWindow)
  }

  createActorItemWindow () {
    const wy = this._commandWindow.y + this._commandWindow.height
    const wh = Graphics.boxHeight - wy
    this._actorItemWindow = new Window_ItemStash(0, wy, Graphics.boxWidth / 2, wh)
    this._actorItemWindow.setHelpWindow(this._helpWindow)
    this._actorItemWindow.setHandler('ok', this.onItemOk.bind(this))
    this._actorItemWindow.setHandler('cancel', this.onItemCancel.bind(this))
    this._actorItemWindow.setActor(this._actor)
    this._actorItemWindow.setStashType('actor')
    this._actorItemWindow.setAlwaysEnabled(true)
    this._actorItemWindow.hide()
    this.addWindow(this._actorItemWindow)
    this._categoryWindow.setTradeWindow(this._actorItemWindow)
  }

  onOkCommand () {
    this.deactivateWindow(this._commandWindow, true)
    this._itemWindow.setShowAllItems(false)

    if (this._commandWindow.currentSymbol() === 'discard') {
      this.activateWindow(this._categoryWindow, true)
      this._categoryWindow.refresh()
    } else {
      this.activateActorSelect()
    }
  }

  onCategoryOk () {
    if (this._commandWindow.currentSymbol() === 'discard' ||
          this._commandWindow.currentSymbol() === 'withdraw') {
      this.activateWindow(this._itemWindow)
      this._itemWindow.selectLast()
    } else {
      this.activateWindow(this._actorItemWindow)
      this._actorItemWindow.selectLast()
    }
  }

  onCategoryCancel () {
    this._itemWindow.deactivate()
    this._categoryWindow.select(0)
    this._categoryWindow.hide()
    this._itemWindow.width = Graphics.boxWidth
    this._itemWindow.x = 0
    this._actorItemWindow.hide()
    this._statusWindow.setActor(null)
    if (this._commandWindow.currentSymbol() !== 'discard') {
      this.activateActorSelect()
    } else {
      this.onActorCancel()
    }
  }

  onActorCancel () {
    this._statusWindow.setForGlobalStash(true)
    this.deactivateActorSelect()
    this.activateWindow(this._commandWindow, true)
    this._itemWindow.setShowAllItems(true)
  }

  onActorOk () {
    super.onActorOk()
    this._actor = this.tradeActor()

    this.activateWindow(this._categoryWindow, true)
    this.deactivateActorSelect()

    this._statusWindow.setForGlobalStash(false)
    this._statusWindow.setActor(this.actor())
    this._actorItemWindow.setActor(this.actor())

    this._itemWindow.width = Graphics.boxWidth / 2
    this._itemWindow.x = this._actorItemWindow.x + this._actorItemWindow.width
    this._itemWindow.setShowAllItems(false)
    this._itemWindow.refresh()
    this._actorItemWindow.show()
  }

  onActorChange () {
    this._statusWindow.setActor(this._actor)
    this._itemWindow.setActor(this._actor)
    this._actorItemWindow.setActor(this._actor)
    this._statusWindow.refresh()
    this._itemWindow.refresh()
    this._actorItemWindow.refresh()
    this._activeWindow.activate()
  }

  item () {
    return this._itemWindow.stashItem()
  }

  onItemOk () {
    let amount = 0
    const commandSymbol = this._commandWindow.currentSymbol()

    if (commandSymbol === 'withdraw' || commandSymbol === 'discard') {
      amount = $gameParty.numItemsInGlobalStash(this.item())
    } else if (commandSymbol === 'deposit') {
      const actorItem = this._actorItemWindow.stashItem()
      amount = this.actor().numItemsInStash(actorItem)
    }

    this._quantityWindow.setMax(amount)

    if (amount === 1) {
      this.onQuantityOk()
      return
    }

    this._activeWindow.deactivate()
    this.activateWindow(this._quantityWindow, true)
  }

  onQuantityOk () {
    super.onQuantityOk()
    const amount = this._quantityWindow.number()

    this.onQuantityCancel()

    switch (this._commandWindow.currentSymbol()) {
      case 'withdraw':
        this.performTradeAction(null, this.actor(), this.stashItem(), amount, () => {
          this._itemWindow.selectLast()
          this.activateWindow(this._itemWindow)
        })
        break

      case 'deposit':
        const actorItem = this._actorItemWindow.stashItem()
        this.performTradeAction(this.actor(), null, actorItem, amount, () => {
          this._actorItemWindow.selectLast()
          this.activateWindow(this._actorItemWindow)
        })
        break

      case 'discard':
        $gameParty.removeFromGlobalStash(this.item(), amount)
        this._itemWindow.selectLast()
        this.activateWindow(this._itemWindow)
        break
    }
    this._statusWindow.refresh()
    this._itemWindow.refresh()
    this._actorItemWindow.refresh()
  }

  onQuantityCancel () {
    super.onQuantityCancel()
    const commandSymbol = this._commandWindow.currentSymbol()

    if (commandSymbol === 'withdraw' || commandSymbol === 'discard') {
      this.activateWindow(this._itemWindow)
    } else {
      this.activateWindow(this._actorItemWindow)
    }
  }

  onItemCancel () {
    this._itemWindow.deactivate()
    this._itemWindow.deselect()
    this._actorItemWindow.deactivate()
    this._actorItemWindow.deselect()
    this.activateWindow(this._categoryWindow)
  }
}
