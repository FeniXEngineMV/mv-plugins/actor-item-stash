import Window_EquipItem from '../Windows/Window_EquipItem'

Scene_Equip.prototype.createItemWindow = function () {
  const wx = 0
  const wy = this._statusWindow.y + this._statusWindow.height
  const ww = Graphics.boxWidth
  const wh = Graphics.boxHeight - wy
  this._itemWindow = new Window_EquipItem(wx, wy, ww, wh)
  this._itemWindow.setHelpWindow(this._helpWindow)
  this._itemWindow.setStatusWindow(this._statusWindow)
  this._itemWindow.setHandler('ok', this.onItemOk.bind(this))
  this._itemWindow.setHandler('cancel', this.onItemCancel.bind(this))
  this._itemWindow.setStashType('actor')
  this._slotWindow.setItemWindow(this._itemWindow)
  this.addWindow(this._itemWindow)
}
