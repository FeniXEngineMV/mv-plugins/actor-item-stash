import Scene_ItemStash from './Scene_ItemStash'
import Scene_GlobalStash from './Scene_GlobalStash'
import { _Params } from '../Core'

Scene_Menu.prototype.createCommandWindow = function () {
  this._commandWindow = new Window_MenuCommand(0, 0)
  this._commandWindow.setHandler('item', this.commandPersonal.bind(this))
  this._commandWindow.setHandler('skill', this.commandPersonal.bind(this))
  this._commandWindow.setHandler('equip', this.commandPersonal.bind(this))
  this._commandWindow.setHandler('status', this.commandPersonal.bind(this))
  if (_Params.addGlobalStashToMenu) {
    this._commandWindow.setHandler('global', this.onPersonalOk.bind(this))
  }
  this._commandWindow.setHandler('formation', this.commandFormation.bind(this))
  this._commandWindow.setHandler('options', this.commandOptions.bind(this))
  this._commandWindow.setHandler('save', this.commandSave.bind(this))
  this._commandWindow.setHandler('gameEnd', this.commandGameEnd.bind(this))
  this._commandWindow.setHandler('cancel', this.popScene.bind(this))
  this.addWindow(this._commandWindow)
}

Scene_Menu.prototype.onPersonalOk = function () {
  switch (this._commandWindow.currentSymbol()) {
    case 'item':
      SceneManager.push(Scene_ItemStash)
      break
    case 'skill':
      SceneManager.push(Scene_Skill)
      break
    case 'equip':
      SceneManager.push(Scene_Equip)
      break
    case 'status':
      SceneManager.push(Scene_Status)
      break
    case 'global':
      SceneManager.push(Scene_GlobalStash)
      break
  }
}
