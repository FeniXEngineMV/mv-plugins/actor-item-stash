import { _Params, _Aliases } from '../Core'
import Window_ActorSelect from '../Windows/TempStash/Window_TempActorSelect'
import Window_TempStash from '../Windows/TempStash/Window_TempStash'
import Window_ItemQuantity from '../Windows/Window_ItemQuantity'

const actorSelectParams = _Params.tempActorSelectWindow
const tempStashParams = _Params.tempStashWindow

_Aliases.sceneMapCreateAllWindows = Scene_Map.prototype.createAllWindows

Scene_Map.prototype.createAllWindows = function () {
  _Aliases.sceneMapCreateAllWindows.call(this)
  this.createActorStashSelectWindow()
  this.createTempStashListWindow()
  this.createQuantityWindow()
}

_Aliases.sceneMapUpdate = Scene_Map.prototype.update

Scene_Map.prototype.update = function () {
  _Aliases.sceneMapUpdate.call(this)
  this.updateTempStash()
}

Scene_Map.prototype.updateTempStash = function () {
  const actorSelectVisible = this._actorStashSelect.visible

  if ($gameParty.tempStashHasItems() === false && actorSelectVisible) {
    $gameParty.closeTempStash()
  }

  if ($gameParty.needsStashAssignment() && !actorSelectVisible) {
    this.openStashAssignment()
  } else if (!$gameParty.needsStashAssignment() && actorSelectVisible) {
    this.closeStashAssignmentWindows()
  }
}

Scene_Map.prototype.createActorStashSelectWindow = function () {
  const x = actorSelectParams.x || 0
  const y = actorSelectParams.y || 0
  this._actorStashSelect = new Window_ActorSelect(x, y)
  this._actorStashSelect.setHandler('ok', this.onActorStashOk.bind(this))
  this._actorStashSelect.setHandler('discard', this.onActorDiscard.bind(this))
  this._actorStashSelect.setHandler('auto', this.onActorAutoStash.bind(this))
  this._actorStashSelect.setHandler('cancel', this.onActorStashCancel.bind(this))
  this._actorStashSelect.hide()
  this._actorStashSelect.deactivate()
  this.addWindow(this._actorStashSelect)
}

Scene_Map.prototype.createTempStashListWindow = function () {
  const x = tempStashParams.x > 0 ? tempStashParams.x : this._actorStashSelect.width
  const y = tempStashParams.y > 0 ? tempStashParams.y : this._actorStashSelect.y

  this._tempStashList = new Window_TempStash(x, y)
  this._tempStashList.setHandler('ok', this.onItemOk.bind(this))
  this._tempStashList.setHandler('cancel', this.onItemCancel.bind(this))
  this._tempStashList.hide()
  this._tempStashList.deactivate()
  this.addWindow(this._tempStashList)
}

Scene_Map.prototype.createQuantityWindow = function () {
  this._quantityWindow = new Window_ItemQuantity(0, 0)
  this._quantityWindow.x = Graphics.boxWidth / 2 - this._quantityWindow.width / 2
  this._quantityWindow.y = Graphics.boxHeight / 2 - this._quantityWindow.height / 2
  this._quantityWindow.setHandler('ok', this.onQuantityOk.bind(this))
  this._quantityWindow.setHandler('cancel', this.onQuantityCancel.bind(this))
  this._quantityWindow.hide()
  this.addChild(this._quantityWindow)
}

Scene_Map.prototype.closeStashAssignmentWindows = function () {
  this._actorStashSelect.hide()
  this._actorStashSelect.deactivate()
  this._tempStashList.hide()
  this._tempStashList.deactivate()
  $gameParty.closeTempStash()
  $gameSystem.enableMenu()
}

Scene_Map.prototype.openStashAssignment = function () {
  this._actorStashSelect.activate()
  this._actorStashSelect.select(0)
  this._actorStashSelect.show()
  this._tempStashList.show()
  this.refreshWindows()
  $gameParty.openTempStash()
  $gameSystem.disableMenu()
}

Scene_Map.prototype.activateTempStashList = function () {
  this._tempStashList.activate()
  this._tempStashList.selectLast()
  this.refreshWindows()
}

Scene_Map.prototype.onActorAutoStash = function () {
  const items = $gameParty.tempItemStash()

  items.forEach(item => {
    const amount = $gameParty.numItemsInTempStash(item)
    $gameParty.autoAddToStash(item, amount)
    $gameParty.removeFromTempStash(item, amount)
  })

  if ($gameParty.tempItemStash().length <= 0) {
    this.closeStashAssignmentWindows()
  } else {
    this._tempStashList.deactivate()
    this._actorStashSelect.activate()
  }
  this.refreshWindows()
}

Scene_Map.prototype.onActorDiscard = function () {
  this._tempStashList.setActorCommandWindow(this._actorStashSelect)
  this.activateTempStashList()
}

Scene_Map.prototype.onActorStashOk = function () {
  const actorSelectSymbol = this._actorStashSelect.currentSymbol()
  if (actorSelectSymbol !== 'auto' || actorSelectSymbol !== 'discard') {
    this.activateTempStashList()
  }
}

Scene_Map.prototype.onActorStashCancel = function () {
  const action = _Params.tempStashCancelAction
  const items = $gameParty.tempItemStash()

  if (action === 'discard') {
    items.forEach(item => $gameParty.removeFromTempStash(item))
  } else if (action === 'auto') {
    items.forEach(item => {
      const amount = $gameParty.numItemsInTempStash(item)
      $gameParty.autoAddToStash(item, amount)
      $gameParty.removeFromTempStash(item, amount)
    })
  }

  if (action !== 'disallow') {
    this.closeStashAssignmentWindows()
    $gameParty.closeTempStash()
  } else {
    SoundManager.playBuzzer()
    this._actorStashSelect.activate()
  }
}

Scene_Map.prototype.onItemOk = function () {
  const stashItem = this._tempStashList.stashItem()
  const amount = $gameParty.numItemsInTempStash(stashItem)

  this._quantityWindow.setMax(amount)

  if (amount === 1) {
    this.onQuantityOk()
    return
  }

  this._tempStashList.deactivate()
  this._quantityWindow.show()
  this._quantityWindow.activate()
}

Scene_Map.prototype.onItemCancel = function () {
  this._tempStashList.deactivate()
  this._actorStashSelect.activate()
}

Scene_Map.prototype.onQuantityOk = function () {
  const actor = $gameActors.actor(this._actorStashSelect.currentExt())
  const stashItem = this._tempStashList.stashItem()
  const amount = this._quantityWindow.number()

  this.onQuantityCancel()

  if (this._actorStashSelect.currentSymbol() === 'discard') {
    $gameParty.removeFromTempStash(stashItem, amount)
    this.refreshWindows()
    this._tempStashList.activate()
    if ($gameParty.tempItemStashAmount() <= 0) {
      this.closeStashAssignmentWindows()
    }
    return
  }
  if (actor.canStashItems(stashItem, amount)) {
    $gameParty.removeFromTempStash(stashItem, amount)
    actor.addToStash(stashItem, amount)
  } else {
    SoundManager.playBuzzer()
  }
  this._tempStashList.refresh()
  this._actorStashSelect.refresh()
}

Scene_Map.prototype.onQuantityCancel = function () {
  this._quantityWindow.deactivate()
  this._quantityWindow.hide()

  this._tempStashList.refresh()
  this._tempStashList.activate()
}

Scene_Map.prototype.refreshWindows = function () {
  this._actorStashSelect.refresh()
  this._actorStashSelect.updateHeight()
  this._tempStashList.refresh()
  this._tempStashList.refreshHeight()
}
