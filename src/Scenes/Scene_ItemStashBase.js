import { _Params } from '../Core'
import Window_MenuActor from '../Windows/Window_MenuActor'
import Window_ItemQuantity from '../Windows/Window_ItemQuantity'
import { itemType } from '../Utils/itemType'

export default class Scene_ItemStashBase extends Scene_ItemBase {
  constructor () {
    super()
    super.initialize()
    this.updateActor()
    this._activeWindow = null
    this._tradeActor = null
  }

  start () {
    super.start()
    this._actorSelectWindow.refresh()
  }

  updateHelp () {}

  update () {
    super.update()
    this.updateHelp()
  }

  tradeActor () {
    return this._tradeActor
  }

  setTradeActor (actor) {
    this._tradeActor = actor
  }

  create () {
    super.create()
    this.createWindowLayer()
    this.createHelpWindow()
  }

  createQuantityWindow () {
    this._quantityWindow = new Window_ItemQuantity(0, 0)
    this._quantityWindow.x = Graphics.boxWidth / 2 - this._quantityWindow.width / 2
    this._quantityWindow.y = Graphics.boxHeight / 2 - this._quantityWindow.height / 2
    this._quantityWindow.setHandler('ok', this.onQuantityOk.bind(this))
    this._quantityWindow.setHandler('cancel', this.onQuantityCancel.bind(this))
    this._quantityWindow.hide()
    this.addChild(this._quantityWindow)
  }

  createActorSelectWindow () {
    this._actorSelectWindow = new Window_MenuActor()
    this._actorSelectWindow.setHandler('ok', this.onActorOk.bind(this))
    this._actorSelectWindow.setHandler('cancel', this.onActorCancel.bind(this))
    this._actorSelectWindow.setCurrentActor(this._actor)
    this._actorSelectWindow.reserveFaceImages()
    this.addWindow(this._actorSelectWindow)
    /**
     * Set default actorWindow to ours because "actorWindow" is too general of
     * a name and determineItem() will not work without it being named that
     */
    this._actorWindow = this._actorSelectWindow
  }

  activeWindow () {
    return this._activeWindow
  }

  activateActorSelect () {
    this._helpWindow.setText(_Params.defaultHelpTerms.actorSelect)
    this._actorSelectWindow.selectLast()
    this._actorSelectWindow.activate()
    this._actorSelectWindow.show()
  }

  deactivateActorSelect () {
    this._actorSelectWindow.deactivate()
    this._actorSelectWindow.hide()
  }

  activateWindow (window, show, windowToClose, hide = false) {
    if (windowToClose) {
      this.deactivateWindow(windowToClose, hide)
    }
    if (show) {
      window.show()
    }
    window.activate()
    window.refresh()
    this._activeWindow = window
  }

  deactivateWindow (window, hide) {
    if (hide) {
      window.hide()
    }
    window.deactivate()
    this._activeWindow = null
  }

  currentQuantity () {
    return this._quantityWindow.number() || 1
  }

  user () {
    return this._actorSelectWindow.currentActor()
  }

  stashItem () {
    const item = this._itemWindow.item()

    return {
      id: item.id,
      type: itemType(item)
    }
  }

  makeMenuTradeActorNext () {
    const members = $gameParty.members()
    let index = members.indexOf(this._tradeActor)

    if (index >= 0) {
      index = (index + 1) % members.length
      this.setTradeActor(members[index])
    } else {
      this.setTradeActor(members[0])
    }
    this.onTradeActorChange()
  }

  makeMenuTradeActorPrevious () {
    const members = $gameParty.members()
    let index = members.indexOf(this._tradeActor)

    if (index >= 0) {
      index = (index + members.length - 1) % members.length
      this.setTradeActor(members[index])
    } else {
      this.setTradeActor(members[0])
    }
    this.onTradeActorChange()
  }

  performTradeAction (giver = null, receiver = null, item = this.itemStash(), amount, callback) {
    if (receiver && receiver.canStashItems(item, amount) === false) {
      SoundManager.playBuzzer()
      return
    }

    SoundManager.playOk()

    if (receiver === null) {
      $gameParty.addToGlobalStash(item, amount)
    } else {
      receiver.addToStash(item, amount)
    }

    if (giver === null) {
      $gameParty.removeFromGlobalStash(item, amount)
    } else {
      giver.removeFromStash(item, amount)
    }

    if (callback) {
      callback.call(this)
    }
  }

  performDiscardAction (target, item, amount) {
    target.removeFromStash(item, amount)
  }

  onTradeActorChange () {}

  onQuantityOk () {
    $gameParty.setLastItem(this.item())
    this.onQuantityCancel()
  }

  onQuantityCancel () {
    this.deactivateWindow(this._quantityWindow, true)
    this.activateWindow(this._itemWindow)
  }

  onActorOk () {
    const selectedActor = $gameParty.members()[this._actorSelectWindow.index()]
    this.setTradeActor(selectedActor)
  }
}
