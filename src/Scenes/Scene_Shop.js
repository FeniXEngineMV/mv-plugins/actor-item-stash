import { _Aliases } from '../Core'
import Window_MenuActor from '../Windows/Window_MenuActor'
import Window_ShopSell from '../Windows/Window_ShopSell'
import { itemType } from '../Utils/'

_Aliases.sceneShopStart = Scene_Shop.prototype.start
Scene_Shop.prototype.start = function () {
  _Aliases.sceneShopStart.call(this)
  this._actorSelectWindow.refresh()
}

_Aliases.sceneShopCreate = Scene_Shop.prototype.create
Scene_Shop.prototype.create = function () {
  _Aliases.sceneShopCreate.call(this)
  this.createActorSelectWindow()
}

Scene_Shop.prototype.createSellWindow = function () {
  const wy = this._categoryWindow.y + this._categoryWindow.height
  const wh = Graphics.boxHeight - wy
  this._sellWindow = new Window_ShopSell(0, wy, Graphics.boxWidth, wh)
  this._sellWindow.setHelpWindow(this._helpWindow)
  this._sellWindow.hide()
  this._sellWindow.setHandler('ok', this.onSellOk.bind(this))
  this._sellWindow.setHandler('cancel', this.onSellCancel.bind(this))
  this._sellWindow.setStashType('actor')
  this._categoryWindow.setItemWindow(this._sellWindow)
  this.addWindow(this._sellWindow)
}

Scene_Shop.prototype.createActorSelectWindow = function () {
  this._actorSelectWindow = new Window_MenuActor()
  this._actorSelectWindow.setHandler('ok', this.onActorOk.bind(this))
  this._actorSelectWindow.setHandler('cancel', this.onActorCancel.bind(this))
  this._actorSelectWindow.hide()
  this._actorSelectWindow.reserveFaceImages()
  this.addWindow(this._actorSelectWindow)
}

Scene_Shop.prototype.onActorOk = function () {
  this._dummyWindow.hide()
  this._buyWindow.setActor(this.user())
  this._statusWindow.setActor(this.user())
  this._sellWindow.setActor(this.user())
  if (this._commandWindow.currentSymbol() === 'buy') {
    this._actorSelectWindow.hide()
    this._actorSelectWindow.deactivate()
    this._buyWindow.select(0)
    this.activateBuyWindow()
  } else if (this._commandWindow.currentSymbol() === 'sell') {
    this._actorSelectWindow.hide()
    this._actorSelectWindow.deactivate()
    this._sellWindow.select(0)
    this.activateSellWindow()
  }
}

Scene_Shop.prototype.user = function () {
  return $gameParty.members()[this._actorSelectWindow.index()]
}

Scene_Shop.prototype.onActorCancel = function () {
  this._actorSelectWindow.hide()
  this._actorSelectWindow.deactivate()
  this._commandWindow.activate()
}

Scene_Shop.prototype.showActorSelectWindow = function (params) {
  this._actorSelectWindow.show()
  this._actorSelectWindow.activate()
}

Scene_Shop.prototype.commandBuy = function () {
  this._commandWindow.deactivate()
  this.showActorSelectWindow()
}

Scene_Shop.prototype.commandSell = function () {
  this._commandWindow.deactivate()
  this.showActorSelectWindow()
}

Scene_Shop.prototype.stashItem = function () {
  const commandSymbol = this._commandWindow.currentSymbol()
  const item = commandSymbol === 'buy' ? this._buyWindow.item() : this._sellWindow.item()
  return {
    id: item.id,
    type: itemType(item)
  }
}

// isSellShop for SRDude SellShop compatibility
Scene_Shop.prototype.isSellShop = function () {
  return typeof $gameTemp.getSellShop !== 'undefined' && $gameTemp.getSellShop()
}

_Aliases.sceneShopOnBuyCancel = Scene_Shop.prototype.onBuyCancel
Scene_Shop.prototype.onBuyCancel = function () {
  if (this.isSellShop()) {
    this._buyWindow.deselect()
    this.showActorSelectWindow()
    this._statusWindow.setItem(null)
    this._helpWindow.clear()
  } else {
    _Aliases.sceneShopOnBuyCancel.call(this)
    this._commandWindow.deactivate()
    this.showActorSelectWindow()
  }
}

Scene_Shop.prototype.maxBuy = function () {
  const max = this.max() < 1 ? 1 : this.max()
  const price = this.buyingPrice()
  if (price > 0) {
    return Math.min(max, Math.floor(this.money() / price))
  } else {
    return max
  }
}

Scene_Shop.prototype.maxSell = function () {
  return this.user().numItemsInStash(this.stashItem())
}

Scene_Shop.prototype.max = function () {
  return this.user().maxItemStack() - this.user().numItemsInStash(this.stashItem())
}

Scene_Shop.prototype.doBuy = function (number) {
  $gameParty.loseGold(number * this.buyingPrice())
  this.user().addToStash(this.stashItem(), number)
}

Scene_Shop.prototype.doSell = function (number) {
  $gameParty.gainGold(number * this.sellingPrice())
  this.user().removeFromStash(this.stashItem(), number)
}
