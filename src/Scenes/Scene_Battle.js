/* globals Yanfly */
import { _Aliases } from '../Core'
import Window_BattleItem from '../Windows/Window_BattleItem'

Scene_Battle.prototype.createItemWindow = function () {
  const wy = this._helpWindow.y + this._helpWindow.height
  const wh = this._statusWindow.y - wy
  this._itemWindow = new Window_BattleItem(0, wy, Graphics.boxWidth, wh)
  this._itemWindow.setHelpWindow(this._helpWindow)
  this._itemWindow.setHandler('ok', this.onItemOk.bind(this))
  this._itemWindow.setHandler('cancel', this.onItemCancel.bind(this))
  this._itemWindow.setActor(BattleManager.actor())
  this._itemWindow.setStashType('actor')
  this.addWindow(this._itemWindow)

  if (typeof Imported.YEP_BattleEngineCore !== 'undefined') {
    /* eslint-disable no-eval */
    if (eval(Yanfly.Param.BECLowerWindows)) {
      this.adjustLowerWindow(this._itemWindow)
    }
  }
}

_Aliases.sceneBattleUpdate = Scene_Battle.prototype.update

Scene_Battle.prototype.update = function () {
  _Aliases.sceneBattleUpdate.call(this)
  if (BattleManager.actor() !== null) {
    if (this._itemWindow._actor !== BattleManager.actor()) {
      this._itemWindow.setActor(BattleManager.actor())
    }
  }
}
