import { _Params } from '../Core'
import Scene_ItemStashBase from '../Scenes/Scene_ItemStashBase'

import Window_ItemStash from '../Windows/ItemStash/Window_ItemStash'
import Window_ItemCommands from '../Windows/ItemStash/Window_ItemStashCommands'
import Window_ItemStashCategory from '../Windows/ItemStash/Window_ItemStashCategory'
import Window_ItemStatus from '../Windows/Window_ItemStatus.js'

/**
 * A new Scene to handle new actor stash/trading and discarding of items
 */
export default class Scene_ItemStash extends Scene_ItemStashBase {
  constructor () {
    super()
    super.initialize()
  }

  start () {
    super.start()
    this._statusWindow.refresh()
  }

  updateHelp () {
    if (this._commandWindow &&
      (this._commandWindow.active || this._categoryWindow.active)) {
      const helpTerms = _Params.defaultHelpTerms

      switch (this._commandWindow.currentSymbol()) {
        case 'use':
          this._helpWindow.setText(helpTerms.use)
          break
        case 'trade':
          this._helpWindow.setText(helpTerms.trade)
          break
        case 'discard':
          this._helpWindow.setText(helpTerms.discard)
          break

        default:
          this._helpWindow.setText(helpTerms.default)
          break
      }
    }
  }

  create () {
    super.create()
    this.createItemCommandsWindow()
    this.createCategoryWindow()
    this.createStatusWindow()
    this.createStatusTradeWindow()
    this.createItemWindow()
    this.createTradeItemWindow()
    this.createActorSelectWindow()
    this.createQuantityWindow()
  }

  createCategoryWindow () {
    this._categoryWindow = new Window_ItemStashCategory()
    this._categoryWindow.setHelpWindow(this._helpWindow)
    this._categoryWindow.y = this._helpWindow.height
    this._categoryWindow.x = 0
    this._categoryWindow.hide()
    this._categoryWindow.deactivate()
    this._categoryWindow.setHandler('ok', this.onCategoryOk.bind(this))
    this._categoryWindow.setHandler('cancel', this.onCategoryCancel.bind(this))
    this._categoryWindow.setHandler('pagedown', this.nextActor.bind(this))
    this._categoryWindow.setHandler('pageup', this.previousActor.bind(this))
    this._categoryWindow.setItemCommandWindow(this._commandWindow)
    this.addWindow(this._categoryWindow)
  }

  createStatusWindow () {
    const wx = this._commandWindow.width
    const wy = this._helpWindow.height
    const ww = Graphics.boxWidth - wx
    const wh = this._commandWindow.height
    this._statusWindow = new Window_ItemStatus(wx, wy, ww, wh)
    this._statusWindow.reserveFaceImages()
    this._statusWindow.setActor(this.actor())
    this.addWindow(this._statusWindow)
  }

  createStatusTradeWindow () {
    const wx = this._commandWindow.width
    const wy = this._helpWindow.height
    const ww = Graphics.boxWidth - wx
    const wh = this._commandWindow.height
    this._tradeStatusWindow = new Window_ItemStatus(wx, wy, ww, wh)
    this._tradeStatusWindow.reserveFaceImages()
    this._tradeStatusWindow.hide()
    this.addWindow(this._tradeStatusWindow)
  }

  createItemCommandsWindow () {
    this._commandWindow = new Window_ItemCommands(0, this._helpWindow.height)
    this._commandWindow.setHelpWindow(this._helpWindow)
    this._commandWindow.setHandler('use', this.onUseCommand.bind(this))
    this._commandWindow.setHandler('trade', this.onTradeCommand.bind(this))
    this._commandWindow.setHandler('discard', this.onDiscardCommand.bind(this))
    this._commandWindow.setHandler('cancel', this.popScene.bind(this))
    this._commandWindow.setHandler('pagedown', this.nextActor.bind(this))
    this._commandWindow.setHandler('pageup', this.previousActor.bind(this))
    this._activeWindow = this._commandWindow
    this.addWindow(this._commandWindow)
  }

  createItemWindow () {
    const wy = this._commandWindow.y + this._commandWindow.height
    const wh = Graphics.boxHeight - wy
    this._itemWindow = new Window_ItemStash(0, wy, Graphics.boxWidth, wh)
    this._itemWindow.setHelpWindow(this._helpWindow)
    this._itemWindow.setHandler('ok', this.onItemOk.bind(this))
    this._itemWindow.setHandler('cancel', this.onItemCancel.bind(this))
    this._itemWindow.setHandler('pagedown', this.nextActor.bind(this))
    this._itemWindow.setHandler('pageup', this.previousActor.bind(this))
    this._itemWindow.setActor(this.actor())
    this._itemWindow.setStashType('actor')
    this._itemWindow.setAlwaysEnabled(true)
    this._itemWindow.setShowAllItems(true)
    this.addWindow(this._itemWindow)
    this._categoryWindow.setItemWindow(this._itemWindow)
  }

  createTradeItemWindow () {
    const wy = this._commandWindow.y + this._commandWindow.height
    const wh = Graphics.boxHeight - wy
    this._tradeWindow = new Window_ItemStash(0, wy, Graphics.boxWidth / 2, wh)
    this._tradeWindow.setActor(null)
    this._tradeWindow.setStashType('actor')
    this._tradeWindow.hide()
    this._tradeWindow.setAlwaysEnabled(true)
    this.addWindow(this._tradeWindow)
    this._categoryWindow.setTradeWindow(this._tradeWindow)
  }

  setupTradeWindows () {
    const statusWidth = (Graphics.boxWidth - this._commandWindow.width) / 2

    this._tradeStatusWindow.setActor(this.tradeActor())
    this._tradeWindow.setActor(this.tradeActor())

    this.deactivateActorSelect()
    this.deactivateWindow(this._commandWindow, true)

    // Update item and trade window dimensions
    this._itemWindow.width = Graphics.boxWidth / 2
    this._tradeWindow.x = this._itemWindow.width
    // Update status window dimensions
    this._statusWindow.width = statusWidth
    this._tradeStatusWindow.width = statusWidth
    this._tradeStatusWindow.x = this._statusWindow.x + this._statusWindow.width

    this._itemWindow.setAlwaysEnabled(true)
    this._itemWindow.setShowAllItems(false)
    this._tradeWindow.refresh()
    this._tradeWindow.show()
    this._tradeStatusWindow.show()
    this.activateWindow(this._categoryWindow, true)
  }

  onUseCommand () {
    this._itemWindow.setAlwaysEnabled(false)
    this._itemWindow.setShowAllItems(false)
    this._categoryWindow.refresh()
    this.activateWindow(this._categoryWindow, true, this._commandWindow, true)
  }

  onTradeCommand () {
    this._categoryWindow.refresh()
    this.activateActorSelect()
  }

  onDiscardCommand () {
    this._itemWindow.setAlwaysEnabled(true)
    this._categoryWindow.refresh()
    this._itemWindow.setShowAllItems(false)
    this.activateWindow(this._categoryWindow, true, this._commandWindow, true)
  }

  onActorCancel () {
    if (this._commandWindow.currentSymbol() !== 'use') {
      this.activateWindow(this._commandWindow, true)
    } else {
      this._activeWindow.activate()
    }
    this.deactivateActorSelect()
    this._itemWindow.refresh()
  }

  onActorOk () {
    super.onActorOk()
    if (this._commandWindow.currentSymbol() === 'trade') {
      if (this.tradeActor() === this.actor()) {
        SoundManager.playBuzzer()
        return
      }
      this.setupTradeWindows()
    } else if (this.canUse()) {
      this.useItem()
    } else {
      SoundManager.playBuzzer()
    }
  }

  nextActor () {
    const currentSymbol = this._commandWindow.currentSymbol()

    if (this._activeWindow === this._commandWindow && currentSymbol === 'trade') {
      super.nextActor()
      return
    }

    if (currentSymbol === 'trade') {
      this.makeMenuTradeActorNext()

      if (this.actor() === this.tradeActor()) {
        this.makeMenuTradeActorNext()
      }
      return
    }

    super.nextActor()
  }

  previousActor () {
    const currentSymbol = this._commandWindow.currentSymbol()
    if (this._activeWindow === this._commandWindow && currentSymbol === 'trade') {
      super.previousActor()
      return
    }

    if (currentSymbol === 'trade') {
      this.makeMenuTradeActorPrevious()

      if (this.actor() === this.tradeActor()) {
        this.makeMenuTradeActorPrevious()
      }
      return
    }

    super.previousActor()
  }

  onActorChange () {
    this._activeWindow.activate()
    this._statusWindow.setActor(this._actor)
    this._itemWindow.setActor(this._actor)
    this._actorSelectWindow.setCurrentActor(this._actor)
    this._statusWindow.refresh()
    this._itemWindow.refresh()
  }

  onTradeActorChange () {
    this._activeWindow.activate()
    this._tradeWindow.setActor(this._tradeActor)
    this._tradeStatusWindow.setActor(this._tradeActor)
  }

  onCategoryOk () {
    this._itemWindow.activate()
    this._itemWindow.selectLast()
    this._activeWindow = this._itemWindow
  }

  onCategoryCancel () {
    if (this._commandWindow.currentSymbol() === 'trade') {
      const statusWidth = Graphics.boxWidth - this._commandWindow.width
      this._itemWindow.width = Graphics.boxWidth
      this._tradeWindow.hide()
      this._tradeStatusWindow.hide()
      this.activateActorSelect()
      this._helpWindow.setText('Select another party member.')
      this._statusWindow.width = statusWidth
    }
    this.deactivateWindow(this._itemWindow)
    this._itemWindow.setShowAllItems(true)
    this._itemWindow.setAlwaysEnabled(true)
    this._itemWindow.refresh()
    this._categoryWindow.select(0)
    this.deactivateWindow(this._categoryWindow, true)
    if (this._commandWindow.currentSymbol() !== 'trade') {
      this.activateWindow(this._commandWindow, true)
    }
  }

  useItem () {
    super.useItem()
    this._itemWindow.redrawCurrentItem()
    this._statusWindow.refresh()
  }

  playSeForItem () {
    SoundManager.playUseItem()
  }

  onItemOk () {
    if (this._commandWindow.currentSymbol() === 'use') {
      this._actorSelectWindow.setForItemUse(true)
      this.determineItem()
    } else {
      const amount = this.actor().numItemsInStash(this.stashItem())
      this._quantityWindow.setMax(amount)

      if (amount === 1) {
        this.onQuantityOk()
        return
      }

      this.activateWindow(this._quantityWindow, true)
    }
  }

  onQuantityOk () {
    super.onQuantityOk()

    switch (this._commandWindow.currentSymbol()) {
      case 'trade':
        this.performTradeAction(
          this.actor(),
          this.tradeActor(),
          this.stashItem(),
          this.currentQuantity()
        )
        break
      case 'discard':
        this.performDiscardAction(this.actor(), this.stashItem(), this.currentQuantity())
        break
    }
    this._itemWindow.refresh()
    this._tradeWindow.refresh()
    this._statusWindow.refresh()
    this._itemWindow.activate()
  }

  onItemCancel () {
    this._itemWindow.deselect()
    this._categoryWindow.activate()
    this._activeWindow = this._categoryWindow
  }
}
