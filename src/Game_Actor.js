import { doubleMatch } from './Utils/core'
import { _Notetags, _Params, _Aliases } from './Core'
import {
  filterDoubleItems,
  removeFromStash,
  addToStash,
  itemType
} from './Utils/'

_Aliases.gameActorSetup = Game_Actor.prototype.setup
Game_Actor.prototype.setup = function (actorId) {
  _Aliases.gameActorSetup.call(this, actorId)
  const actorNotetag = _Notetags.actors[actorId]
  this._itemStash = []
  this._itemStashSlots = actorNotetag ? actorNotetag.startingSlots : _Params.actorStashSlots
  this._maxItemStack = actorNotetag ? actorNotetag.startingStack : _Params.actorItemStack
}

Game_Actor.prototype.itemStashData = function (type) {
  const stash = this.itemStash()
  const matchType = (item, type) => {
    if (item.type === type) {
      return item
    }
  }
  const items = stash.filter(i => matchType(i, 'item')).map(j => $dataItems[j.id])
  const weapons = stash.filter(i => matchType(i, 'weapon')).map(j => $dataWeapons[j.id])
  const armors = stash.filter(i => matchType(i, 'armor')).map(j => $dataArmors[j.id])

  switch (type) {
    case 'items': return items
    case 'weapons': return weapons
    case 'armors': return armors
    default: return items.concat(armors).concat(weapons)
  }
}

Game_Actor.prototype.itemStash = function () {
  return filterDoubleItems(this._itemStash)
}

Game_Actor.prototype.itemStashAmount = function () {
  return this._itemStashSlots - this.itemStash().length
}

Game_Actor.prototype.addToStash = function (item, amount = 1) {
  if (this.canStashItems(item, amount)) {
    addToStash(item, amount, this._itemStash, this._itemStashSlots)
  }
}

Game_Actor.prototype.removeFromStash = function (item, amount = 1) {
  removeFromStash(item, amount, this._itemStash)
}

Game_Actor.prototype.numItemsInStash = function (item) {
  return this._itemStash.filter(
    stashItem => doubleMatch(item.id, stashItem.id, item.type, stashItem.type)
  ).length
}

Game_Actor.prototype.canStashItems = function (item, amount) {
  if (item) {
    const itemOwned = this.numItemsInStash(item) > 0
    if (itemOwned) {
      return this.canStackItem(item, amount)
    }
    return this.itemStashSlotsUsed() < this._itemStashSlots && this.canStackItem(item, amount)
  } else {
    return this.itemStashSlotsUsed() < this._itemStashSlots
  }
}

Game_Actor.prototype.canStackItem = function (item, amount = 1) {
  return amount + this.numItemsInStash(item) <= this.maxItemStack()
}

Game_Actor.prototype.setMaxStashSlots = function (value) {
  this._itemStashSlots = value
}

Game_Actor.prototype.setMaxItemStack = function (value) {
  this._maxItemStack = value
}

Game_Actor.prototype.maxItemStack = function (value) {
  return this._maxItemStack
}

Game_Actor.prototype.maxItemStashSlots = function (value) {
  return this._itemStashSlots
}

Game_Actor.prototype.itemStashSlotsUsed = function () {
  if (_Params.forceStackAsSlots) {
    return this._itemStash.length
  } else {
    return this.itemStash().length
  }
}

Game_Actor.prototype.changeEquip = function (slotId, item) {
  const autoAdd = (item) => {
    if (this.canStashItems(item)) {
      this.addToStash(itemToUnequip)
    } else {
      $gameParty.autoAddToStash(item)
    }
  }

  const itemToUnequip = {
    id: this._equips[slotId].itemId(),
    type: this._equips[slotId]._dataClass
  }

  const canUnequip = itemToUnequip.id > 0

  // If swapping out equipped item with an empty slot (clear command)
  if (!item && canUnequip) {
    const canMoveItem = this.canStashItems(itemToUnequip) || $gameParty.canAddToGlobalStash(1)

    if (canMoveItem) {
      autoAdd(itemToUnequip)
      this._equips[slotId].setObject(item)
    }
    this.refresh()
  } else if (item) {
    const itemToEquip = { id: item.id, type: itemType(item) }
    const isItemOwned = this.numItemsInStash(itemToEquip) > 0

    // If both item from slot and item in inventory are the same
    if (itemToUnequip.id === item.id) {
      this._equips[slotId].setObject(null)
      autoAdd(itemToEquip)
      return
    }

    if (isItemOwned && this.equipSlots()[slotId] === item.etypeId) {
      this.removeFromStash(itemToEquip)
      // only add to stash if there is an item to unequip
      if (canUnequip) {
        autoAdd(itemToUnequip)
      }
      this._equips[slotId].setObject(item)
      this.refresh()
    }
  }
}

Game_Actor.prototype.bestEquipItem = function (slotId) {
  const etypeId = this.equipSlots()[slotId]
  const equipItems = this.itemStashData('armors').concat(this.itemStashData('weapons'))
  const items = equipItems.filter((item) => {
    return item.etypeId === etypeId && this.canEquip(item)
  })
  let bestItem = null
  let bestPerformance = -1000

  for (let i = 0; i < items.length; i++) {
    const performance = this.calcEquipItemPerformance(items[i])
    if (performance > bestPerformance) {
      bestPerformance = performance
      bestItem = items[i]
    }
  }
  return bestItem
}
