import { _Aliases } from './Core'

_Aliases.gamePlayerCanMove = Game_Player.prototype.canMove

Game_Player.prototype.canMove = function () {
  if ($gameParty.isActorSelectOpen()) {
    return false
  }
  return _Aliases.gamePlayerCanMove.call(this)
}
