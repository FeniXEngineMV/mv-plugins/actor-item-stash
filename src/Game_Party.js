import { _Params, _Aliases } from './Core'
import { doubleMatch } from './Utils/core'

import {
  filterDoubleItems,
  removeFromStash,
  addToStash,
  itemType
} from './Utils/'

/**
 * Game_Party
 *
 * A stash item is an item to be added to an actors stash, all items received
 * will be assigned to a temp stash this temp stash will reset upon leaving a
 * map.
 *
 * Overwrites:
 * numItems()
 * allItems()
 *
 * Aliases:
 * gainItem
 *
 */
_Aliases.gamePartyInitialize = Game_Party.prototype.initialize

Game_Party.prototype.initialize = function () {
  _Aliases.gamePartyInitialize.call(this)
  this._tempItemStash = []
  this._globalItemStash = []
  this._maxGlobalStashSlots = _Params.maxGlobalStash
  this._maxTempStashSlots = _Params.maxTempStash
  this._isActorSelectOpen = false
  this._needsStashAssignment = false
}

_Aliases.gamePartyGainItem = Game_Party.prototype.gainItem

Game_Party.prototype.gainItem = function (item, amount, includeEquip) {
  _Aliases.gamePartyGainItem.call(this, item, amount, includeEquip)
  const currentScene = SceneManager._scene.constructor.name.toString()

  if (!item) { return }
  const stashItem = { id: item.id, type: itemType(item) }

  if (currentScene === 'Scene_Battle' && amount > 0) {
    if (_Params.autoAddAfterBattle) {
      this.autoAddToStash(stashItem, amount)
      return
    }
  } else if ((currentScene === 'Scene_Map') && amount > 0) {
    this.addToTempStash(stashItem, amount)
  } else if (amount < 0) {
    this.autoRemoveFromStash(stashItem, Math.abs(amount))
  }
}

Game_Party.prototype.numItems = function (item) {
  return this.allStashItems().filter(_item => {
    return doubleMatch(_item.id, item.id, _item.type, itemType(item))
  }).length
}

Game_Party.prototype.allItems = function () {
  const result = []
  this.members().forEach(actor => result.push(...actor.itemStashData()))
  return result.filter(result => result)
}

Game_Party.prototype.allStashItems = function () {
  const result = []
  this.members().forEach(actor => result.push(...actor._itemStash))
  return result.filter(result => result)
}

Game_Party.prototype.globalItemStash = function () {
  return filterDoubleItems(this._globalItemStash)
}

Game_Party.prototype.globalItemStashSlotsUsed = function () {
  if (_Params.forceStackAsSlots) {
    return this._globalItemStash.length
  } else {
    return this.globalItemStash()
  }
}

Game_Party.prototype.tempItemStash = function () {
  return filterDoubleItems(this._tempItemStash)
}

Game_Party.prototype.stashItemsToAssign = function () {
  return this._tempItemStash
}

Game_Party.prototype.tempStashHasItems = function () {
  return this._tempItemStash.length > 0
}

Game_Party.prototype.needsStashAssignment = function () {
  return this.tempStashHasItems() && this._needsStashAssignment
}

Game_Party.prototype.openTempStash = function () {
  this._needsStashAssignment = true
}

Game_Party.prototype.closeTempStash = function () {
  this._needsStashAssignment = false
}

Game_Party.prototype.tempItemStashAmount = function () {
  return this.tempItemStash().length
}

Game_Party.prototype.numItemsInTempStash = function (item) {
  return this._tempItemStash.filter(
    stashTtem => doubleMatch(item.id, stashTtem.id, item.type, stashTtem.type)
  ).length
}

Game_Party.prototype.numItemsInGlobalStash = function (item) {
  return this._globalItemStash.filter(
    stashTtem => doubleMatch(item.id, stashTtem.id, item.type, stashTtem.type)
  ).length
}

Game_Party.prototype.autoAddToStash = function (item, amount = 1) {
  const actorsWithSpace = this.members().filter(member => member.canStashItems(item, amount))
  if (actorsWithSpace.length > 0) {
    actorsWithSpace[0].addToStash(item, amount)
  } else {
    this.addToGlobalStash(item, amount)
  }
}

Game_Party.prototype.autoRemoveFromStash = function (item, amount = 1) {
  const itemOwners = this.members().filter(member => {
    return member.itemStash().some(
      _item => doubleMatch(_item.id, item.id, _item.type, item.type))
  })

  if (itemOwners.length > 0) {
    itemOwners[0].removeFromStash(item, amount)
  } else {
    this.removeFromGlobalStash(item, amount)
  }
}

Game_Party.prototype.canAddToGlobalStash = function (amount = 1) {
  if ((this.globalItemStashSlotsUsed() + amount) <= this._maxGlobalStashSlots) {
    return true
  }
  return false
}

Game_Party.prototype.addToGlobalStash = function (item, amount = 1) {
  if (this.canAddToGlobalStash(amount)) {
    addToStash(item, amount, this._globalItemStash)
  }
}

Game_Party.prototype.removeFromGlobalStash = function (itemId, amount = 1) {
  removeFromStash(itemId, amount, this._globalItemStash)
}

Game_Party.prototype.addToTempStash = function (item, amount = 1) {
  if (this.tempItemStash().length < this._maxTempStashSlots) {
    addToStash(item, amount, this._tempItemStash)
  }
}

Game_Party.prototype.removeFromTempStash = function (item, amount = 1) {
  removeFromStash(item, amount, this._tempItemStash)
}

Game_Party.prototype.setActorSelectOpen = function (boolean) {
  this._isActorSelectOpen = boolean
}

Game_Party.prototype.isActorSelectOpen = function () {
  return this._isActorSelectOpen
}

Game_Party.prototype.setMaxGlobalStashSlots = function (value) {
  this._maxGlobalStashSlots = value
}

Game_Party.prototype.setMaxTempStash = function (value) {
  this._maxTempStashSlots = value
}
