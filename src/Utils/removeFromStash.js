import { times, doubleMatch } from './core/index'

export function removeFromStash (item, amount, stash) {
  times(amount, () => {
    let removed = false

    stash.forEach((stashItem, i) => {
      if (removed) { return }
      doubleMatch(item.id, stashItem.id, item.type, stashItem.type, () => {
        stash.splice(i, 1)
        removed = true
      })
    })
  })
}
