export function times (amount, action) {
  for (let i = amount; i > 0; i--) {
    action(i)
  }
}
