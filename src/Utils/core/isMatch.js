export function isMatch (a, b, cb) {
  if (a === b) {
    if (cb) {
      return cb()
    }
    return true
  }
}
