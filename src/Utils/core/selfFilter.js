/**
 * Filters an array by comparing with action function
 *
 * @param {array} array The array to filter
 * @param {function} action A function to compare with the array
 */
export function selfFilter (array, action) {
  return array.filter(
    (item, index, self) => index === self.findIndex(t => action(t, item)))
}
