export function doubleMatch (a, b, c, d, action) {
  if (a === b && c === d) {
    if (action) { action() }
    return true
  }
  return false
}
