import { doubleMatch, selfFilter } from './core/'

export function filterDoubleItems (arr) {
  return selfFilter(arr, (prev, current) => {
    return doubleMatch(prev.id, current.id, prev.type, current.type)
  })
}
