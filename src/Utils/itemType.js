export function itemType (item) {
  if (DataManager.isItem(item)) {
    return 'item'
  } else if (DataManager.isWeapon(item)) {
    return 'weapon'
  } else if (DataManager.isArmor(item)) {
    return 'armor'
  } else {
    return null
  }
}
