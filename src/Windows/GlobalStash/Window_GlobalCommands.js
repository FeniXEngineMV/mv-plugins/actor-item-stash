import { _Params } from '../../Core'
export default class Window_GlobalCommands extends Window_Command {
  constructor (x, y) {
    super()
    super.initialize(x, y)
  }

  windowHeight () {
    return this.fittingHeight(4)
  }

  makeCommandList () {
    const terms = _Params.defaultTerms
    this.addCommand(terms.withdrawTerm, 'withdraw', true)
    this.addCommand(terms.depositTerm, 'deposit', true)
    this.addCommand(terms.discardTerm, 'discard', true)
  }
}
