import { _Aliases, _Params } from '../Core'

if (_Params.addGlobalStashToMenu) {
  _Aliases.addMainCommands = Window_MenuCommand.prototype.addMainCommands

  Window_MenuCommand.prototype.addMainCommands = function () {
    _Aliases.addMainCommands.call(this)
    const enabled = this.areMainCommandsEnabled()

    if (this.needsCommand('global')) {
      this.addCommand(_Params.globalMenuName, 'global', enabled)
    }
  }
}
