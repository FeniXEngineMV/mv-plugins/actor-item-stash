import { _Params } from '../Core'

export default class extends Window_Selectable {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, this.windowWidth(), 145)
    this.createButtons()
  }

  windowWidth () {
    return 300
  }

  number () {
    return this._number
  }

  setMax (max) {
    this._number = 1
    this._max = max
    this.refresh()
  }

  createButtons () {
    const bitmap = ImageManager.loadSystem('ButtonSet')
    this.downButton = new Sprite_Button()
    this.downButton.bitmap = bitmap
    this.downButton.setColdFrame(48, 0, 48, 48)
    this.downButton.setHotFrame(48, 48, 48, 48)
    this.downButton.x = 24
    this.downButton.y = this.itemY() + 10
    this.downButton.setClickHandler(this.onDownButton.bind(this))

    this.upButton = new Sprite_Button()
    this.upButton.bitmap = bitmap
    this.upButton.setColdFrame(48 * 2, 0, 48, 48)
    this.upButton.setHotFrame(48 * 2, 48, 48, 48)
    this.upButton.x = this.downButton.x + 50
    this.upButton.y = this.itemY() + 10
    this.upButton.setClickHandler(this.onUpButton.bind(this))

    this.okButton = new Sprite_Button()
    this.okButton.bitmap = bitmap
    this.okButton.setColdFrame(48 * 4, 0, 48 * 2, 48)
    this.okButton.setHotFrame(48 * 4, 48, 48 * 2, 48)
    this.okButton.x = this.upButton.x + 50
    this.okButton.y = this.itemY() + 10
    this.okButton.setClickHandler(this.onOkButton.bind(this))
    this.addChild(this.upButton)
    this.addChild(this.downButton)
    this.addChild(this.okButton)
  }

  drawNumber () {
    const x = this.cursorX()
    const y = this.itemY()
    const width = this.cursorWidth() - this.textPadding()
    this.resetTextColor()
    this.drawText(this._number, x, y, width, 'right')
  }

  update () {
    super.update()
    this.processNumberChange()
  }

  refresh () {
    this.contents.clear()
    super.refresh()
    this.drawNumber()
    this.drawText(_Params.defaultTerms.quantityTerm, 0, 0, this.contentsWidth(), 'center')
  }

  itemY () {
    return 48
  }

  cursorWidth () {
    const digitWidth = this.textWidth('0')
    return this.maxDigits() * digitWidth + this.textPadding() * 2
  }

  cursorX () {
    return this.contentsWidth() - this.cursorWidth() - this.textPadding()
  }

  updateCursor () {
    this.setCursorRect(this.cursorX(), this.itemY(),
      this.cursorWidth(), this.lineHeight())
  }

  maxDigits () {
    return 2
  }

  isOkTriggered () {
    return Input.isTriggered('ok')
  }

  playOkSound () {}

  processNumberChange () {
    if (this.isOpenAndActive()) {
      if (Input.isRepeated('right')) {
        this.changeNumber(1)
      }
      if (Input.isRepeated('left')) {
        this.changeNumber(-1)
      }
      if (Input.isRepeated('up')) {
        this.changeNumber(10)
      }
      if (Input.isRepeated('down')) {
        this.changeNumber(-10)
      }
    }
  }

  changeNumber (amount) {
    const lastNumber = this._number
    this._number = (this._number + amount).clamp(1, this._max)
    if (this._number !== lastNumber) {
      SoundManager.playCursor()
      this.refresh()
    }
  }

  onDownButton () {
    this.changeNumber(-1)
  }

  onUpButton () {
    this.changeNumber(1)
  }

  onOkButton () {
    this.processOk()
  }
}
