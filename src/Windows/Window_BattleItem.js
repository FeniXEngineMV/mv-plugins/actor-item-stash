import Window_ItemStash from '../Windows/ItemStash/Window_ItemStash.js'

export default class extends Window_ItemStash {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
    super.hide()
  }

  includes (item) {
    return $gameParty.canUse(item)
  }

  show () {
    this.selectLast()
    this.showHelpWindow()
    this.visible = true
  }

  hide () {
    this.hideHelpWindow()
    this.visible = false
  }
}
