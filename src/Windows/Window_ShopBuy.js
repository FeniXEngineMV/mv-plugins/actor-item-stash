import { itemType } from '../Utils/'

Window_ShopBuy.prototype.sellShopExists = function () {
  return typeof $gameTemp.getSellShop !== 'undefined'
}

Window_ShopBuy.prototype.canPurchase = function (item) {
  return item && this.price(item) <= this._money
}

Window_ShopBuy.prototype.isEnabled = function (item) {
  const stashItem = {
    id: item.id,
    type: itemType(item)
  }

  if (!this.sellShopExists() || (this.sellShopExists() && !$gameTemp.getSellShop())) {
    return this._actor && this._actor.canStashItems(stashItem) && this.canPurchase(item)
  } else if (this.sellShopExists() && $gameTemp.getSellShop()) {
    /**
     * Since SRDude's sell shop plugin uses the buy window as a sell window we can only enable
     * item if the amount in possession by actor is greater than 0
     */
    return this._actor && this._actor.numItemsInStash(stashItem) > 0
  }
}

Window_ShopBuy.prototype.setActor = function (actor) {
  this._actor = actor
  this.refresh()
}

Window_ShopBuy.prototype.actor = function () {
  return this._actor
}
