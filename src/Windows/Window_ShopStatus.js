import { itemType } from '../Utils/'

Window_ShopStatus.prototype.drawPossession = function (x, y) {
  if (!this._actor) {
    return
  }
  const width = this.contents.width - this.textPadding() - x
  const maxStack = this._actor.maxItemStack()
  const numItems = this._actor.numItemsInStash({
    id: this._item.id,
    type: itemType(this._item)
  })
  const possessionWidth = this.textWidth('0000')
  this.changeTextColor(this.systemColor())
  this.drawText(TextManager.possession, x, y, width - possessionWidth)
  this.resetTextColor()
  this.drawText(`${numItems}/${maxStack}`, x, y, width, 'right')
}

Window_ShopStatus.prototype.setActor = function (actor) {
  this._actor = actor
  this.refresh()
}

Window_ShopStatus.prototype.actor = function () {
  return this._actor
}
