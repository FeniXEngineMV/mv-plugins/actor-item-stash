import { _Params } from '../../Core'

import Window_ItemStashBase from '../ItemStash/Window_ItemStashBase'

export default class Window_TempStash extends Window_ItemStashBase {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, this.windowWidth(), height)
    this._actor = null
    this._actorCommandWindow = null
    this.refresh()
  }

  windowWidth () {
    return _Params.tempStashWindow.width || 400
  }

  maxCols () {
    return _Params.tempStashWindow.maxCols || super.maxCols()
  }

  numVisibleRows () {
    return _Params.tempStashWindow.numVisibleRows || this._data.length
  }

  windowHeight () {
    return this.fittingHeight(this.numVisibleRows())
  }

  refreshHeight () {
    this.height = this.windowHeight()
    super.refresh()
  }

  setActorCommandWindow (window) {
    this._actorCommandWindow = window
    this.refresh()
  }

  includes () {
    return true
  }

  isCurrentItemEnabled () {
    return this.isEnabled(this.item())
  }

  isEnabled (item) {
    if (this._actorCommandWindow && this._actorCommandWindow.currentSymbol() === 'discard') {
      return (DataManager.isItem(item) && item.itypeId === 2) === false
    }
    return true
  }

  makeItemList () {
    this._data = this.parseStashData($gameParty.tempItemStash())
  }

  standardFontSize () {
    return _Params.tempStashWindow.fontSize || super.standardFontSize()
  }

  drawItem (index) {
    const rect = this.itemRect(index)
    const item = this._data[index]
    const amount = $gameParty.numItemsInTempStash(this.stashItem(index))
    this.changePaintOpacity(this.isEnabled(item))
    this.drawIcon(item.iconIndex, rect.x, rect.y)
    this.drawText(item.name, rect.x + 36, rect.y, rect.width)
    this.drawText(`x${amount}`, rect.x, rect.y, rect.width, 'right')
    this.changePaintOpacity(1)
  }
}
