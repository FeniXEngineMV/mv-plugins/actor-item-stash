import { _Params } from '../../Core'

export default class Window_TempActorSelect extends Window_Command {
  constructor (x, y) {
    super()
    super.initialize(x, y)
  }

  windowWidth () {
    return _Params.tempActorSelectWindow.width || super.windowWidth()
  }

  numVisibleRows () {
    return _Params.tempActorSelectWindow.numVisibleRows || super.numVisibleRows()
  }

  standardFontSize () {
    return _Params.tempActorSelectWindow.fontSize || super.standardFontSize()
  }

  updateHeight () {
    this.height = this.windowHeight()
    super.refresh()
  }

  refresh () {
    super.refresh()
  }

  isCommandEnabled (actor) {
    return actor.canStashItems() && $gameParty.tempItemStashAmount() > 0
  }

  makeCommandList () {
    const terms = _Params.defaultTerms
    this.addCommand(terms.autoTerm, 'auto', true)
    this.addCommand(terms.discardTerm, 'discard', true)
    $gameParty.members().forEach(actor => {
      this.addCommand(
        actor.name(),
        actor.name().toLowerCase(),
        this.isCommandEnabled(actor),
        actor.actorId()
      )
    })
  }

  show () {
    super.show()
    $gameParty.setActorSelectOpen(true)
  }

  hide () {
    super.hide()
    $gameParty.setActorSelectOpen(false)
  }

  drawItem (index) {
    const actor = $gameParty.members().filter(
      actor => actor && actor.name() === this.commandName(index))[0]
    const rect = this.itemRect(index)
    const align = this.itemTextAlign()
    if (actor) {
      this.resetTextColor()
      this.changePaintOpacity(this.isCommandEnabled(actor))
      this.drawCharacter(actor, rect.x + 20, rect.y)
      this.drawText(this.commandName(index), rect.x + 42, rect.y, rect.width, align)
      this.drawText(actor.itemStashAmount(), rect.x, rect.y, rect.width, 'right')
    } else {
      this.drawIcon(16, rect.x, rect.y)
      this.drawText(this.commandName(index), rect.x + 36, rect.y, rect.width)
    }
  }

  drawCharacter (actor, x, y) {
    const characterName = actor.characterName()
    const characterIndex = actor.characterIndex()
    const bitmap = ImageManager.loadCharacter(characterName)
    const big = ImageManager.isBigCharacter(characterName)
    bitmap.addLoadListener((bitmap) => {
      const pw = bitmap.width / (big ? 3 : 12)
      const ph = bitmap.height / (big ? 4 : 8)
      const n = characterIndex
      const sx = (n % 4 * 3 + 1) * pw
      const sy = (Math.floor(n / 4) * 4) * ph
      this.contents.blt(bitmap, sx, sy, pw, ph - 16, x - pw / 2, y)
    })
  }
}
