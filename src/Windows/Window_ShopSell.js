import Window_ItemStash from '../Windows/ItemStash/Window_ItemStash'

export default class extends Window_ItemStash {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
  }

  isEnabled (item) {
    return item && item.price > 0
  }
}
