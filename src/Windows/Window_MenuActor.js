import { _Params } from '../Core'

export default class extends Window_MenuActor {
  constructor (x, y) {
    super(x, y)
    super.initialize(x, y)
    this.select(0)
    this._currentActor = null
    this._forItemUse = false
  }

  currentActor () {
    return this._currentActor
  }

  setCurrentActor (actor) {
    this._currentActor = actor
    this.refresh()
  }

  setForItemUse (boolean) {
    this._forItemUse = boolean
    this.refresh()
  }

  drawItemImage (index) {
    const actor = $gameParty.members()[index]
    const rect = this.itemRect(index)

    if (_Params.altMenuEnabled) {
      // Code taken from AltMenu plugin
      const bitmapName = $dataActors[actor.actorId()].meta.stand_picture
      const bitmap = bitmapName ? ImageManager.loadPicture(bitmapName) : null
      const w = Math.min(rect.width, (bitmapName ? bitmap.width : 144))
      const h = Math.min(rect.height, (bitmapName ? bitmap.height : 144))
      const lineHeight = this.lineHeight()
      if (this._forItemUse) {
        this.changePaintOpacity(actor.isBattleMember())
      } else {
        this.changePaintOpacity(actor !== this._currentActor)
      }
      if (bitmap) {
        const sx = (bitmap.width > w) ? (bitmap.width - w) / 2 : 0
        const sy = (bitmap.height > h) ? (bitmap.height - h) / 2 : 0
        const dx = (bitmap.width > rect.width) ? rect.x
          : rect.x + (rect.width - bitmap.width) / 2
        const dy = (bitmap.height > rect.height) ? rect.y
          : rect.y + (rect.height - bitmap.height) / 2
        this.contents.blt(bitmap, sx, sy, w, h, dx, dy)
      } else { // when bitmap is not set, do the original process.
        this.drawActorFace(actor, rect.x, rect.y + lineHeight * 2.5, w, h)
      }
      this.changePaintOpacity(true)
    } else {
      // Default behaviour
      const fw = Window_Base._faceWidth
      if (this._forItemUse) {
        this.changePaintOpacity(actor.isBattleMember())
      } else {
        this.changePaintOpacity(actor !== this._currentActor)
      }
      this.drawActorFace(actor, rect.x + 1, rect.y + 1, fw, rect.height - 2)
      this.changePaintOpacity(true)
    }
  }

  drawItemStatus (index) {
    const actor = $gameParty.members()[index]
    if (!this._forItemUse) {
      this.changePaintOpacity(actor !== this._currentActor)
    }
    super.drawItemStatus(index)
    this.changePaintOpacity(true)
  }
}
