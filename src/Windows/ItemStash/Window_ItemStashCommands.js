import { _Params } from '../../Core'

export default class Window_ItemStashCommands extends Window_Command {
  constructor (x, y) {
    super()
    super.initialize(x, y)
  }

  windowHeight () {
    return this.fittingHeight(4)
  }

  // isCommandEnabled (actor) {
  //   return actor.canStashItems() && $gameParty.tempItemStashAmount() > 0
  // }

  makeCommandList () {
    const terms = _Params.defaultTerms
    this.addCommand(terms.useTerm, 'use', true)
    this.addCommand(terms.tradeTerm, 'trade', true)
    this.addCommand(terms.discardTerm, 'discard', true)
  }
}
