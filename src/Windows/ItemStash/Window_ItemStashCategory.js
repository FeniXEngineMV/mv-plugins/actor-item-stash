export default class Window_ItemStashCategory extends Window_Command {
  constructor (x, y) {
    super()
    super.initialize(x, y, this.windowWidth(), this.windowHeight())
    this._itemCommandWindow = null
  }

  windowWidth () {
    return 240
  }

  setItemCommandWindow (window) {
    this._itemCommandWindow = window
    this.refresh()
  }

  update () {
    super.update()
    if (this._itemWindow) {
      this._itemWindow.setCategory(this.currentSymbol())
    }
    if (this._tradeWindow) {
      this._tradeWindow.setCategory(this.currentSymbol())
    }
  }

  isCategoryEnabled () {
    if (!this._itemCommandWindow) {
      return
    }
    return this._itemCommandWindow.currentSymbol() !== 'discard'
  }

  makeCommandList () {
    this.addCommand(TextManager.item, 'item')
    this.addCommand(TextManager.weapon, 'weapon')
    this.addCommand(TextManager.armor, 'armor')
    this.addCommand(TextManager.keyItem, 'keyItem', this.isCategoryEnabled())
  }

  setItemWindow (itemWindow) {
    this._itemWindow = itemWindow
  }

  setTradeWindow (tradeWindow) {
    this._tradeWindow = tradeWindow
  }
}
