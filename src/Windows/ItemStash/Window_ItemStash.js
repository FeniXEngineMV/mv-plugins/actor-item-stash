import Window_ItemStashBase from './Window_ItemStashBase'

export default class Window_Stash extends Window_ItemStashBase {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
  }

  maxCols () {
    return this.width > Graphics.boxWidth / 2 ? 2 : 1
  }

  includes (item) {
    if (!item) {
      return false
    }

    if (this._showAllItems) {
      return true
    }

    switch (this._category) {
      case 'item':
        return DataManager.isItem(item) && item.itypeId === 1
      case 'weapon':
        return DataManager.isWeapon(item)
      case 'armor':
        return DataManager.isArmor(item)
      case 'keyItem':
        return DataManager.isItem(item) && item.itypeId === 2
      default:
        return false
    }
  }

  drawItem (index) {
    const item = this._data[index]
    if (item) {
      const amount = this.numItemsInStash(this.stashItem(index))
      const numberWidth = this.numberWidth()
      const rect = this.itemRect(index)
      rect.width -= this.textPadding()
      this.changePaintOpacity(this.isEnabled(item))

      this.drawItemName(item, rect.x, rect.y, rect.width - numberWidth)
      this.drawText(`x${amount}`, rect.x, rect.y, rect.width, 'right')
      this.changePaintOpacity(1)
    }
  }

  makeItemList () {
    this._data = this.stashData().filter((stashItem) => this.includes(stashItem))
    if (this.includes(null)) {
      this._data.push(null)
    }
  }
}
