import { itemType } from '../../Utils/itemType'

function itemDataArray (type) {
  switch (type) {
    case 'armor': return $dataArmors
    case 'weapon': return $dataWeapons
    case 'item': return $dataItems
    default: return null
  }
}

export default class Window_ItemStashBase extends Window_ItemList {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
    this._isAlwaysEnabled = false
    this._stashType = null
    this._showAllItems = false
  }

  update () {
    super.update()
  }

  refresh () {
    super.refresh()
  }

  /**
  * We overwrite reselect to ensure the item is not empty before re-selecting
  * it to circumvent MV's bad behavior of selecting an empty slot.
  */
  reselect () {
    const item = this._index > -1 ? this._data[this._index] : null
    if (item) {
      this.select(this._index)
    } else {
      const newIndex = this._index > 0 ? this._index - 1 : this._index
      this.select(newIndex)
    }
  }

  setActor (actor) {
    this._actor = actor
    this.refresh()
  }

  setStashType (type) {
    this._stashType = type
    this.refresh()
  }

  setShowAllItems (boolean) {
    this._showAllItems = boolean
    this.refresh()
  }

  isAlwaysEnabled () {
    return this._isAlwaysEnabled
  }

  setAlwaysEnabled (boolean) {
    this._isAlwaysEnabled = boolean
    this.refresh()
  }

  isEnabled (item) {
    return item && this.isAlwaysEnabled() ? true : $gameParty.canUse(item)
  }

  /**
   * Because the object used for an actors item stash is a simple container with
   * only the item id and the type. We need to fetch the correct $dataItems object
   * and return a an array of default MV items data.
   *
   * @param {*} stashItems - An array of stash item objects
   */

  parseStashData (stashItems) {
    return stashItems.map(stashItem =>
      itemDataArray(stashItem.type)[stashItem.id])
  }

  stashData () {
    switch (this._stashType) {
      case 'actor': return this._actor ? this.parseStashData(this._actor.itemStash()) : []
      case 'global': return this.parseStashData($gameParty.globalItemStash())
      case 'temp': return this.parseStashData($gameParty.tempItemStash())
      default: return []
    }
  }

  numItemsInStash (stashItem) {
    if (!stashItem) { return }
    switch (this._stashType) {
      case 'actor': return this._actor.numItemsInStash(stashItem)
      case 'global': return $gameParty.numItemsInGlobalStash(stashItem)
      case 'temp': return $gameParty.numItemsInTempStash(stashItem)
      default: return 0
    }
  }

  stashItem (index) {
    const item = index >= 0 ? this._data[index] : this.item()
    if (item) {
      return {
        id: item.id,
        type: itemType(item)
      }
    }
  }
}
