import { _Params } from '../Core'

export default class extends Window_SkillStatus {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
    this._isGlobalStash = false
  }

  setForGlobalStash (boolean = true) {
    this._isGlobalStash = boolean
    this.refresh()
  }

  isForGlobalStash () {
    return this._isGlobalStash
  }

  drawActorLevel (actor, x, y) {
    this.changeTextColor(this.systemColor())
    this.drawText(TextManager.levelA, x, y, 48)
    this.resetTextColor()
    this.drawText(actor.level, x + 15, y, 36, 'right')
  }

  drawGlobalStashStatus (x, y) {
    const terms = _Params.defaultTerms
    const width = this.width
    const x2 = x + this.textWidth(terms.globalItemsUsedTerm) + this.standardPadding()
    const y2 = y + this.lineHeight()
    const maxSlots = $gameParty._maxGlobalStashSlots
    const slotsUsed = $gameParty.globalItemStashSlotsUsed()
    this.drawText(terms.globalItemsUsedTerm, x, y, width, 'left')
    this.drawText(terms.globalMaxItemsTerm, x, y2, width, 'left')
    this.drawText(slotsUsed, x2, y, width, 'left')
    this.drawText(maxSlots, x2, y2, width, 'left')
  }

  drawActorItemStashStatus (x, y) {
    const terms = _Params.defaultTerms
    const width = this.width - 170
    const x2 = x + this.textWidth(terms.actorItemsUsedTerm) + this.standardPadding()
    const y2 = y + this.lineHeight()
    const maxSlots = this._actor.maxItemStashSlots()
    const maxItemStack = this._actor.maxItemStack()
    const slotsUsed = this._actor.itemStashSlotsUsed()
    this.drawText(terms.actorItemsUsedTerm, x, y, width, 'left')
    this.drawText(terms.actorMaxItemsTerm, x, y2, width, 'left')
    this.drawText(terms.actorItemStackTerm, x, y2 + this.lineHeight(), width, 'left')
    this.drawText(slotsUsed, x2, y, width, 'left')
    this.drawText(maxSlots, x2, y2, width, 'left')
    this.drawText(maxItemStack, x2, y2 + this.lineHeight(), width, 'left')
  }

  refresh () {
    this.contents.clear()
    const h = this.height - this.padding * 2
    const y = h / 2 - this.lineHeight() * 1.5

    if (this._actor) {
      this.drawActorFace(this._actor, 0, 0, 144, h)
      this.contents.fontSize = this.standardFontSize() - 4
      this.drawActorName(this._actor, 162, y)
      this.drawActorLevel(this._actor, 162, y + this.lineHeight() * 1)
      this.drawActorItemStashStatus(280, y)
      this.contents.fontSize = this.standardFontSize()
    } else if (this.isForGlobalStash()) {
      this.contents.fontSize = this.standardFontSize() - 4
      this.drawGlobalStashStatus(0, y)
    }
  }
}
