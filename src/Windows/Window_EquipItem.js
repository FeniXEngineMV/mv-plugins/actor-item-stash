import Window_ItemStash from '../Windows/ItemStash/Window_ItemStash'
import { itemType } from '../Utils'

export default class extends Window_ItemStash {
  constructor (x, y, width, height) {
    super()
    super.initialize(x, y, width, height)
  }

  setSlotId (slotId) {
    if (this._slotId !== slotId) {
      this._slotId = slotId
      this.refresh()
      this.resetScroll()
    }
  }

  includes (item) {
    if (item === null) {
      return true
    }

    if (this._slotId < 0 || item.etypeId !== this._actor.equipSlots()[this._slotId]) {
      return false
    }
    return this._actor.canEquip(item)
  }

  isEnabled (item) {
    const equipSlotItem = this._actor.equips()[this._slotId]
    return !equipSlotItem || (!item && equipSlotItem) || (item && !equipSlotItem) || (this._actor.canStashItems({ id: item.id, type: itemType(item) }) && equipSlotItem === item)
  }

  reselect () {
    Window_Selectable.prototype.reselect.call(this)
  }

  selectLast () {}

  setStatusWindow (statusWindow) {
    this._statusWindow = statusWindow
    this.callUpdateHelp()
  }

  updateHelp () {
    super.updateHelp()
    if (this._actor && this._statusWindow) {
      const actor = JsonEx.makeDeepCopy(this._actor)
      actor.forceChangeEquip(this._slotId, this.item())
      this._statusWindow.setTempActor(actor)
    }
  }
}
